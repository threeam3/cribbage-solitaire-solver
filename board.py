#Costanti
ROWS = 4
ROW_LEN = 13
MAX_STACK_VAL = 31
TARGET_SCORE = 61
cardList = list("1234567890JQK")
tenCards = list("0JQK")
streakPoints = {2:2,3:6,4:12}

def getCardValue(card):
	if card is None : return 0
	if cardList.count(card) <= 0 : raise ValueError("Card " + str(card) +" is invalid")
	if tenCards.count(card) > 0  : return 10
	return cardList.index(card) + 1

def appendList(l1,l2):
	for i in l2:
		l1.append(i)


class CardStack:
	cards = []
	moves = []
	total = 0
	value = 0

	def __init__(self):
		self.cards = []
		self.moves = []
		self.total = 0
		self.value = 0

	def __str__(self) -> str:
		s = ''.join(self.cards) + " " 
		s += ''.join(map(str,self.moves)) + " "
		s = s + str(self.total) + " " + str(self.value)
		return s

	def addCard(self,card):
		if not self.canAddCards() : 
			print(str(self))
			raise ValueError("Stack is full!")
		self.total += getCardValue(card)
		self.cards.append(card)

	def addMove(self,move):
		if not isinstance(move,int) : raise ValueError("Invalid move! (Not an int)")
		if move < 0 : raise ValueError("Move < 0")
		if move >= ROWS : raise ValueError("Move out of bounds!")
		self.moves.append(move)

	def canAddCards(self):
		return self.total < MAX_STACK_VAL

	def canAdd(self,card):
		if card is None : 
			#print("Card was none")
			return False
		if not self.canAddCards() : return False
		return self.total + getCardValue(card) <= MAX_STACK_VAL

	def getStackValue(self, doPrint = False):
		if len(self.cards) <= 0 :
			self.value = 0
			return 0

		self.cards.append(None)
		val = 0

		#-Jack iniziale
		if self.cards[0] == 'J' : val += 2
		partialTotal = 0
		lastCard = None
		streak = 1
		runMin = cardList.index(self.cards[0])
		runMax = runMin
		for card in self.cards:
			partialTotal += getCardValue(card)
			if doPrint : print(partialTotal)
			# Stack a 15 o 31
			if (partialTotal == 15 or partialTotal == 31 ) and card is not None: 
				val += 2

			# Coppia, tris o poker
			if card == lastCard:
				streak += 1
			else :
				lastCard = card
				val += streakPoints.get(streak,0)
				streak = 1

			#Scala
			cardOrder = -2
			if card is not None : cardOrder = cardList.index(card)

			if cardOrder == runMin - 1 : runMin = cardOrder
			elif cardOrder == runMax + 1 : runMax = cardOrder
			else:
				run = runMax - runMin + 1
				runMax = cardOrder
				runMin = cardOrder
				if run>=3 : val += run
		self.value = val
		self.cards.pop()
		return val

	# Restituisce il valore in memoria
	def getVal(self):
		return self.value

	def copy(self):
		s = CardStack()
		for card in self.cards:
			s.addCard(card)
		for move in self.moves:
			s.addMove(move)
		return s

class Board:
	rows = []

	def __init__(self,cards = ""):
		self.rows = []

		if not isinstance(cards,str) : raise ValueError("Input must be string!")
		cards = cards.replace(' ','')
		if cards != "":
			cards = list(cards)
			if len(cards) != ROWS*ROW_LEN : raise ValueError("Incorrect number of cards! "+str(len(cards)))

		for i in range(ROWS):
			row = []
			self.rows.append(row)

			if cards != "":
				for j in range(ROW_LEN):
					row.append(cards[(i+1)*ROW_LEN - (j+1)])

	def copy(self):
		newBoard = Board()
		for i in range(ROWS):
			for j in range(len(self.rows[i])):
				newBoard.rows[i].append(self.rows[i][j])
		return newBoard

	def isEmpty(self,index):
		return len(self.rows[index]) <= 0
	def isEmptyAll(self):
		for i in range(ROWS):
			if not self.isEmpty(i) : return False
		return True
	def popRow(self,index):
		if self.isEmpty(index) : raise ValueError("Stack is empty!")
		return self.rows[index].pop()
	def topRow(self,index):
		if self.isEmpty(index) : return None
		return self.rows[index][-1]

	def getPossibleMoves(self,baseStack = None):
		moveList = []
		isBaseLevel = baseStack is None
		if isBaseLevel : baseStack = CardStack()

		hasDelved = False

		for i in range(len(self.rows)):
			if baseStack.canAdd(self.topRow(i)):
				#print("Trovata mossa valida")
				hasDelved = True
				boardCopy = self.copy()
				#print("Autocopiatura effettuata")
				stackCopy = baseStack.copy()
				stackCopy.addCard(boardCopy.popRow(i))
				stackCopy.addMove(i)

				appendList(moveList,boardCopy.getPossibleMoves(stackCopy))
			#else : print("Can't add card from "+str(i))

		if not hasDelved : moveList.append(baseStack)

		if isBaseLevel :
			for move in moveList:
				move.getStackValue()
		
		moveList.sort(key=CardStack.getVal,reverse=True)
		
		return moveList

