# Cribbage Solitaire solver

Simple solver for the Mobius Front '83 minigame "Cribbage solitaire"

WARNING: For now, the solving algorithm only explores the stacks with the highest score, so it may not produce a solution
