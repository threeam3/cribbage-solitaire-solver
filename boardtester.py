import sys
from board import *

print("Valid chars: "+ "".join(cardList))

s = ""
for i in range(ROWS):
    isValid = False
    while not isValid:
        row = input("Insert row "+ str(i+1)+ ": ")
        if len(row) != ROW_LEN : print("Invalid row!")
        else : 
            isValid = True
            s += row

x = Board(cards=s)

scoreTotal = 0
print("\nPossible solution found:\n")

while not x.isEmptyAll():
    moves = x.getPossibleMoves()
    bestMove = moves[0]
    scoreTotal += bestMove.value
    print(''.join(map(str,bestMove.moves)))

    for move in bestMove.moves:
        x.popRow(move)

print("\nTotal score: " + str(scoreTotal))
if scoreTotal < TARGET_SCORE : print("WARNING: Score is insufficient for victory")
else : print("Score is sufficient to win")